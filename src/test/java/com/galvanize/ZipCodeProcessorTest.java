package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ZipCodeProcessorTest {

    // write your tests here
    @Test
    public void shouldReturnOutOfRange() {
        Verifier ver = new Verifier();
        ZipCodeProcessor test1 = new ZipCodeProcessor(ver);
        test1.process("10000");
        assertEquals("We're sorry, but the zip code you entered is out of our range.", test1.process("10000"));
    }

    @Test
    public void shouldReturnWrongLength()
    {
        Verifier ver2 = new Verifier();
        ZipCodeProcessor test2 = new ZipCodeProcessor(ver2);
        test2.process("321");
        assertEquals("The zip code you entered was the wrong length.", test2.process("321"));
    }

    @Test
    public void shouldReturnNormalOutput()
    {
        Verifier ver3 = new Verifier();
        ZipCodeProcessor test3 = new ZipCodeProcessor(ver3);
        test3.process("43827");
        assertEquals("Thank you!  Your package will arrive soon.", test3.process("43827"));
    }


}